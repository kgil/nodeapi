"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const httpErrors_1 = require("@utils/errors/httpErrors");
function checkBodyForm(form, body) {
    form.map(item => {
        if (!(item in body)) {
            throw new httpErrors_1.HTTP400Error(`Missing ${item} key`);
        }
        return;
    });
}
exports.default = checkBodyForm;
