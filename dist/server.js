"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("module-alias/register");
require("reflect-metadata");
const http_1 = __importDefault(require("http"));
const express_1 = __importDefault(require("express"));
const typeorm_1 = require("typeorm");
const utils_1 = require("./utils");
const redis_1 = __importDefault(require("./utils/redis"));
const middleware_1 = __importDefault(require("./middleware"));
const services_1 = __importDefault(require("./services"));
const errorHandlers_1 = __importDefault(require("@middleware/errorHandlers"));
process.on('uncaughtException', e => {
    console.log(e);
    process.exit(1);
});
process.on('unhandledRejection', e => {
    console.log(e);
    process.exit(1);
});
const router = express_1.default();
const { PORT = 3001, IP } = process.env;
const server = http_1.default.createServer(router);
exports.redisClient = redis_1.default();
(() => __awaiter(this, void 0, void 0, function* () {
    try {
        yield typeorm_1.createConnection().then(conn => {
        });
    }
    catch (error) {
        console.log('Error while connecting to the database', error);
        return error;
    }
    utils_1.applyMiddleware(middleware_1.default, router);
    utils_1.applyRoutes(services_1.default, router);
    utils_1.applyMiddleware(errorHandlers_1.default, router);
    server.listen(PORT, () => console.log(`Server is running http://${IP || 'localhost'}:${PORT}...`));
}))();
