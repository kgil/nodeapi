"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.bodyProfileKeys = [
    'firstName',
    'lastName',
    'phone',
    'middleName',
    'gender',
    'bornCountry',
    'birthDate',
    'currentCountry',
    'maritalStatus',
    'countChildren',
    'occupation',
    'levelEducation',
    'photo'
];
