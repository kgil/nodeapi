import { BodyUser } from '../types';
import { getRepository } from 'typeorm';
import User, { UserRole } from '@entity/User';
import * as METHODS from 'password-hash';

const createUser = async (body: BodyUser, admin: boolean = false) => {
  let { email, password } = body;
  password = password.trim();
  email = email.toLowerCase().trim();
  const hashedPassword = METHODS.generate(password);

  const user = await getRepository(User).create({
    email,
    password: hashedPassword,
    role: admin ? UserRole.ADMIN: UserRole.GHOST
  }).save();
  return user;
};

export default createUser;