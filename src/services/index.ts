import userRoutes from './user/routes';
import profileRoutes from './profile/routes';
import sessionsRoutes from './session/routes';

export default  [
    ...userRoutes,
    ...profileRoutes,
    ...sessionsRoutes
];