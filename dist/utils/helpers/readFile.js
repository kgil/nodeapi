"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
const readFile = (filePath, nameFile) => {
    return new Promise((resolve, reject) => {
        fs_1.default.readFile(path_1.default.join(`${process.cwd()}/${filePath}`, nameFile), 'utf8', function (err, source) {
            if (err) {
                reject(err);
            }
            resolve(source);
        });
    });
};
exports.default = readFile;
