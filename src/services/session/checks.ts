import { NextFunction } from 'express';
import { getRepository } from "typeorm";
import { HTTP400Error, HTTP401Error } from '@utils/errors/httpErrors';
import * as TOKEN_ACTIONS from '@utils/token';
import User from '@entity/User';
import * as METHODS from 'password-hash';
import { AuthRequest, refreshRequest, createTokenKeys, refreshTokenKeys, deleteTokenKeys } from './types';
import REGEXPS from '@config/regexps';
import checkBodyForm from '@utils/checkRequiredFields';

export const checkDataFromReq = (
  req: AuthRequest,
  res: Response,
  next: NextFunction
) => {
  const email: string = req.body.email.toLowerCase().trim();
  const password: string = req.body.password.trim();
  if (!email || !password) {
    throw new HTTP400Error('You must provide all necessery information');
  } else if (!email.match(REGEXPS.email)) {
    throw new HTTP400Error('Email is not valid');
  } else {
    next();
  }
};

export const checkRequiredFieldsStartSession = (
  { body }: AuthRequest,
  res: Response,
  next: NextFunction
) => {
  checkBodyForm(createTokenKeys, body);
  next();
};

export const checkRequiredFieldsRefreshSession = (
  { body }: AuthRequest,
  res: Response,
  next: NextFunction
) => {
  checkBodyForm(refreshTokenKeys, body);
  next();
};

export const checkRequiredFieldsDeleteSession = (
  { body }: AuthRequest,
  res: Response,
  next: NextFunction
) => {
  checkBodyForm(deleteTokenKeys, body);
  next();
};

export const checkExistingUser = async (
  req: AuthRequest,
  res: Response,
  next: NextFunction
) => {
  const email: string = req.body.email.toLowerCase().trim();

  const existingUser = await getRepository(User)
    .createQueryBuilder('user')
    .where('user.email = :email', { email })
    .getOne();

  if (!existingUser) {
    throw new HTTP400Error('There is not such user');
  } else {
    req.user = existingUser;
    next();
  }
};

export const checkPasswordUser = (
  req: AuthRequest,
  res: Response,
  next: NextFunction
) => {
  const password: string = req.body.password.trim();
  const passwordFromDB: string = req.user.password;

  if (!METHODS.verify(password, passwordFromDB)) {
    throw new HTTP400Error('Password is not valid');
  } else {
    next();
  }
};

export const checkTokens = async (
  req: refreshRequest,
  res: Response,
  next: NextFunction
) => {
  const refreshToken = req.body.refreshToken;
  const accessToken = req.body.accessToken;
  const tokens = await TOKEN_ACTIONS.refreshTokens(accessToken, refreshToken);
  if (!refreshToken || !accessToken || !tokens) {
    throw new HTTP401Error();
  } else {
    next();
  }
};