"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv_1 = __importDefault(require("dotenv"));
const JWT = __importStar(require("jsonwebtoken"));
const httpErrors_1 = require("./errors/httpErrors");
const server_1 = require("../server");
const User_1 = require("@entity/User");
dotenv_1.default.config();
const SECRET_KEY = process.env.SECRET_KEY;
const SECRET_REFRESH_KEY = process.env.SECRET_REFRESH_KEY;
const EXPIRED_ACCES_TOKEN_TIME = 700;
const EXPIRED_REFRESH_TOKEN_TIME = 7000;
exports.createTokens = (id, role = User_1.UserRole.GHOST) => {
    const refreshToken = generateRefreshToken(128);
    const accessToken = JWT.sign({ id, role }, SECRET_KEY, { expiresIn: EXPIRED_ACCES_TOKEN_TIME });
    server_1.redisClient.setex(refreshToken, EXPIRED_REFRESH_TOKEN_TIME, `${String(id)},${String(role)}`);
    return {
        accessToken,
        refreshToken
    };
};
exports.validateToken = (accessToken) => {
    return new Promise((resolve) => {
        JWT.verify(accessToken, SECRET_KEY, (err, decoded) => {
            if (err) {
                if (err.name === 'TokenExpiredError') {
                    throw new httpErrors_1.HTTP440Error();
                }
                else {
                    throw new httpErrors_1.HTTP400Error(err);
                }
            }
            else {
                const data = {
                    flag: true,
                    role: decoded.role,
                    id: decoded.id
                };
                resolve(data);
            }
        });
    });
};
exports.refreshTokens = (accessToken, refreshToken) => {
    return new Promise((resolve) => {
        JWT.verify(accessToken, SECRET_KEY, (err) => __awaiter(this, void 0, void 0, function* () {
            if (err) {
                if (err.name === 'TokenExpiredError') {
                    const data = yield helpRefreshToken(refreshToken);
                    resolve(data);
                }
                else {
                    throw new httpErrors_1.HTTP401Error();
                }
            }
            else {
                const data = {
                    accessToken,
                    refreshToken
                };
                resolve(data);
            }
        }));
    });
};
exports.deleteToken = (refreshToken) => {
    server_1.redisClient.del(refreshToken);
};
const helpRefreshToken = (refreshToken) => __awaiter(this, void 0, void 0, function* () {
    try {
        const response = yield server_1.redisClient.getAsync(refreshToken);
        const arrResponse = response.split(',');
        const id = Number(arrResponse[0]);
        const role = arrResponse[1];
        server_1.redisClient.setex(refreshToken, 20, response);
        return exports.createTokens(id, role);
    }
    catch (e) {
        return null;
    }
});
const generateRefreshToken = (length) => {
    let text = '';
    const charset = SECRET_REFRESH_KEY;
    for (var i = 0; i < length; i++)
        text += charset.charAt(Math.floor(Math.random() * charset.length));
    return text;
};
