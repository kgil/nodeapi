import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, CreateDateColumn, OneToOne, JoinColumn } from 'typeorm';
import Profile from './Profile';

export enum UserRole {
  ADMIN = 'admin',
  GHOST = 'ghost'
}

@Entity('users')
export default class User extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;


    @Column({
      nullable: true
    })
    id_google: string;

    @Column({
      unique: true
    })
    email: string

    @Column()
    password: string

    @Column({
      type: 'enum',
      enum: UserRole,
      default: UserRole.GHOST
    })
    role: UserRole

    @CreateDateColumn({ name: 'created_at', nullable: true })
    createdAt: Date;
    
    @OneToOne(type => Profile, profile => profile.user)
    @JoinColumn()
    profile: Profile;
}
