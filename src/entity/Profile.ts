import { Entity, BaseEntity, PrimaryGeneratedColumn, Column, UpdateDateColumn, OneToOne } from 'typeorm';
import User from './User';

export enum MaritalStatus {
  MARRIED = 'married',
  UNMARRIED = 'unmarried'
}

export enum LevelEducation {
  HIGH = 'high',
  LOW = 'low'
}

@Entity('profiles')
export default class Profile extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    firstName: string;

    @Column()
    lastName: string;

    @Column()
    phone: string;

    @Column()
    middleName: string;

    @Column()
    gender: string;

    @Column()
    bornCountry: string;

    @Column()
    birthDate: string;

    @Column()
    currentCountry: string;

    @Column()
    maritalStatus: string;

    @Column()
    countChildren: number;

    @Column()
    occupation: string;

    @Column()
    levelEducation: string;

    @Column()
    photo: string;

    @UpdateDateColumn({ name: 'updated_at', nullable: true })
    updatedAt: Date;

    @OneToOne(type => User, user => user.profile, {
      cascade: true
    })
    user: User;
}