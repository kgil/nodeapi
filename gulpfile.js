'use strict';
const gulp = require('gulp');

gulp.task('copyViews', () => (
	gulp.src('src/views/**/*')
		.pipe(gulp.dest('dist/views'))
));

gulp.task('copyConfigs', () => (
	gulp.src(['src/config/*', '!src/config/*.ts'])
		.pipe(gulp.dest('dist/config'))
));

gulp.task('default', gulp.series('copyViews', 'copyConfigs'));