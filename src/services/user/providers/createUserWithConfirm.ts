import { BodyUser } from '../types';
import { sendEncodingUser, decodingUser } from '@utils/mailers/confirmEmail';
import createUser from './createUser';
import { HTTP400Error } from '@utils/errors/httpErrors';

export const sendUser = (body: BodyUser) => {
  sendEncodingUser(body);
};

export const confirmEmail = (token: string) => {
  const user = decodingUser(token);
  if (user) {
    return user;
  } else {
    throw new HTTP400Error();
  }
};

export const createUserAfterConfirm = async (user: BodyUser) => {
  try {
    return await createUser(user);
  } catch(e) {
    throw new HTTP400Error(e);
  }
};

