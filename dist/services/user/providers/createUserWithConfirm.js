"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const confirmEmail_1 = require("@utils/mailers/confirmEmail");
const createUser_1 = __importDefault(require("./createUser"));
const httpErrors_1 = require("@utils/errors/httpErrors");
exports.sendUser = (body) => {
    confirmEmail_1.sendEncodingUser(body);
};
exports.confirmEmail = (token) => {
    const user = confirmEmail_1.decodingUser(token);
    if (user) {
        return user;
    }
    else {
        throw new httpErrors_1.HTTP400Error();
    }
};
exports.createUserAfterConfirm = (user) => __awaiter(this, void 0, void 0, function* () {
    try {
        return yield createUser_1.default(user);
    }
    catch (e) {
        throw new httpErrors_1.HTTP400Error(e);
    }
});
