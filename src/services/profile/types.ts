import { Request } from 'express';

export interface BodyProfile {
  firstName: string;
  lastName: string;
  phone: string;
  middleName: string;
  gender: string;
  bornCountry: string;
  birthDate: string;
  currentCountry: string;
  maritalStatus: string;
  countChildren: number;
  occupation: string;
  levelEducation: string;
  photo: string;
}

export const bodyProfileKeys = [
  'firstName',
  'lastName',
  'phone',
  'middleName',
  'gender',
  'bornCountry',
  'birthDate',
  'currentCountry',
  'maritalStatus',
  'countChildren',
  'occupation',
  'levelEducation',
  'photo'
]

export interface ProfileRequest extends Request {
  body: BodyProfile;
  id: number;
}
