"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const METHODS = __importStar(require("password-hash"));
const resetPass_1 = require("@utils/mailers/resetPass");
const User_1 = __importDefault(require("@entity/User"));
const httpErrors_1 = require("@utils/errors/httpErrors");
exports.sendEmail = (email) => {
    resetPass_1.sendEncodingEmail(email);
};
exports.confirmEmail = (token) => {
    const email = resetPass_1.decodingEmail(token);
    if (email) {
        return email;
    }
    else {
        throw new httpErrors_1.HTTP400Error();
    }
};
exports.resetPasswordAfterConfirm = (email, newPassword) => __awaiter(this, void 0, void 0, function* () {
    const hashedPassword = METHODS.generate(newPassword);
    try {
        yield typeorm_1.getConnection()
            .createQueryBuilder()
            .update(User_1.default)
            .set({ password: hashedPassword })
            .where("email = :email", { email })
            .execute();
    }
    catch (e) {
        throw new httpErrors_1.HTTP400Error(e);
    }
});
