import { API_URL } from '@config/commonSettings';
import { Request, Response } from 'express';
import * as CHECKS_USER from './checks';
import * as CONFIRM_EMAIL from './providers/createUserWithConfirm';
import * as RESET_PASSWORD from './providers/resetPass';
import createUser from './providers/createUser';
import authAdmin from '@middleware/authAdmin';
import auth from '@middleware/auth';
import * as USER_FIND_METHODS from './providers/findUserMethods';
import { UserRequest, SendForResetRequest } from './types';

export default [
  {
    path: `${API_URL}user/create/admin`,
    method: 'post',
    handler: [
      CHECKS_USER.checkRequiredFieldsCreateUser,
      CHECKS_USER.checkDataFromReq,
      CHECKS_USER.checkUserForUnique,
      async ({ body }: UserRequest, res: Response) => {
        const user = await createUser(body, true);
        res.status(200).send(res.json({ id: user.id }));
      } 
    ]
  },
  {
    path: `${API_URL}user/create`,
    method: 'post',
    handler: [
      CHECKS_USER.checkRequiredFieldsCreateUser,
      CHECKS_USER.checkDataFromReq,
      CHECKS_USER.checkUserForUnique,
      ({ body }: UserRequest, res: Response) => {
        CONFIRM_EMAIL.sendUser(body);
        res.status(200).send();
      } 
    ]
  },
  {
    path: `${API_URL}confirmEmail/:token`,
    method: 'get',
    handler: [
      async ({ params }: Request, res: Response) => {
        const user = CONFIRM_EMAIL.confirmEmail(params.token);
        await CONFIRM_EMAIL.createUserAfterConfirm(user);
        res.status(200).send();
      } 
    ]
  },
  {
    path: `${API_URL}user/forgot`,
    method: 'post',
    handler: [
      CHECKS_USER.checkRequiredFieldsForSendReset,
      CHECKS_USER.checkEmail,
      CHECKS_USER.checkExistingUser,
      (req: SendForResetRequest, res: Response) => {
        RESET_PASSWORD.sendEmail(req.body.email);
        res.status(200).send();
      }   
    ]
  },
  {
    path: `${API_URL}passwordReset/:token`,
    method: 'post',
    handler: [
      CHECKS_USER.checkRequiredFieldsForResetPasswordWithoutProfile,
      CHECKS_USER.checkEqualPasswords,
      async ({ body, params }: Request, res: Response) => {
        const email = RESET_PASSWORD.confirmEmail(params.token);
        await RESET_PASSWORD.resetPasswordAfterConfirm(email, body.password);
        res.status(200).send();
      } 
    ]
  },
  {
    path: `${API_URL}user/passwordReset`,
    method: 'post',
    handler: [
      auth,
      CHECKS_USER.checkRequiredFieldsForResetPasswordWithProfile,
      CHECKS_USER.checkPassword,
      CHECKS_USER.checkEqualPasswords,
      (req: SendForResetRequest, res: Response) => {
        RESET_PASSWORD.sendEmail(req.body.email);
        res.status(200).send();
      }   
    ]
  },
  {
    path: `${API_URL}user/get`,
    method: 'get',
    handler: [
      CHECKS_USER.checkSearchParams,
      authAdmin,
      async (req: Request, res: Response) => {
        const user = await USER_FIND_METHODS.findUserById(req.query.id)
        res.status(200).send(res.json({ user }));
      }
    ]
  },
  {
    path: `${API_URL}user/get/all`,
    method: 'get',
    handler: [
      authAdmin,
      async (req: Request, res: Response) => {
        const users = await USER_FIND_METHODS.findAllUsers();
        res.status(200).send(res.json({ users }));
      }
    ]
  }
];