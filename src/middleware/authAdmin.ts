import { Request, Response, NextFunction } from 'express';
import { HTTP401Error } from '@utils/errors/httpErrors';
import { UserRole } from '@entity/User';
import { validateToken } from '@utils/token';

const authAdmin = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  if (req.headers.authorization) {
    const bearerToken = req.headers.authorization;
    if (bearerToken.startsWith('Bearer ')) {
      const accessToken = bearerToken.slice(7, bearerToken.length);
      const data =  await validateToken(accessToken);
      if (data.id && data.flag && data.role === UserRole.ADMIN) {
        next();
      }
    } else {
      throw new HTTP401Error();
    }
  } else {
    throw new HTTP401Error();
  }
};

export default authAdmin;