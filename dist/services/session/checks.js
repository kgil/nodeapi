"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const httpErrors_1 = require("@utils/errors/httpErrors");
const TOKEN_ACTIONS = __importStar(require("@utils/token"));
const User_1 = __importDefault(require("@entity/User"));
const METHODS = __importStar(require("password-hash"));
const types_1 = require("./types");
const regexps_1 = __importDefault(require("@config/regexps"));
const checkRequiredFields_1 = __importDefault(require("@utils/checkRequiredFields"));
exports.checkDataFromReq = (req, res, next) => {
    const email = req.body.email.toLowerCase().trim();
    const password = req.body.password.trim();
    if (!email || !password) {
        throw new httpErrors_1.HTTP400Error('You must provide all necessery information');
    }
    else if (!email.match(regexps_1.default.email)) {
        throw new httpErrors_1.HTTP400Error('Email is not valid');
    }
    else {
        next();
    }
};
exports.checkRequiredFieldsStartSession = ({ body }, res, next) => {
    checkRequiredFields_1.default(types_1.createTokenKeys, body);
    next();
};
exports.checkRequiredFieldsRefreshSession = ({ body }, res, next) => {
    checkRequiredFields_1.default(types_1.refreshTokenKeys, body);
    next();
};
exports.checkRequiredFieldsDeleteSession = ({ body }, res, next) => {
    checkRequiredFields_1.default(types_1.deleteTokenKeys, body);
    next();
};
exports.checkExistingUser = (req, res, next) => __awaiter(this, void 0, void 0, function* () {
    const email = req.body.email.toLowerCase().trim();
    const existingUser = yield typeorm_1.getRepository(User_1.default)
        .createQueryBuilder('user')
        .where('user.email = :email', { email })
        .getOne();
    if (!existingUser) {
        throw new httpErrors_1.HTTP400Error('There is not such user');
    }
    else {
        req.user = existingUser;
        next();
    }
});
exports.checkPasswordUser = (req, res, next) => {
    const password = req.body.password;
    const passwordFromDB = req.user.password;
    if (!METHODS.verify(password, passwordFromDB)) {
        throw new httpErrors_1.HTTP400Error('Password is not valid');
    }
    else {
        next();
    }
};
exports.checkTokens = (req, res, next) => __awaiter(this, void 0, void 0, function* () {
    const refreshToken = req.body.refreshToken;
    const accessToken = req.body.accessToken;
    const tokens = yield TOKEN_ACTIONS.refreshTokens(accessToken, refreshToken);
    if (!refreshToken || !accessToken || !tokens) {
        throw new httpErrors_1.HTTP401Error();
    }
    else {
        next();
    }
});
