import User from '@entity/User';
import { getRepository } from 'typeorm';

export const getProfile = async (id: number) => {
  const users = await getRepository(User)
  .find({ select: ['id', 'profile'], relations: ['profile'], where: { id }, take: 1 });
  return users[0].profile;
};