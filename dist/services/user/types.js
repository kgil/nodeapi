"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.bodyUserKeys = [
    'email',
    'password'
];
exports.sendForResetKeys = [
    'email'
];
exports.resetPasswordKeys = [
    'password',
    'confirmPassword'
];
