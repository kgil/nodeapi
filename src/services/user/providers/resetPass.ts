import { getConnection } from 'typeorm';
import * as METHODS from 'password-hash';
import { sendEncodingEmail, decodingEmail } from '@utils/mailers/resetPass';
import User from '@entity/User';
import { HTTP400Error } from '@utils/errors/httpErrors';

export const sendEmail = (email: string) => {
  sendEncodingEmail(email);
};

export const confirmEmail = (token: string) => {
  const email: string = decodingEmail(token);
  if (email) {
    return email;
  } else {
    throw new HTTP400Error();
  }
};

export const resetPasswordAfterConfirm = async (email: string, newPassword: string) => {
  const hashedPassword = METHODS.generate(newPassword);
  try {
    await getConnection()
    .createQueryBuilder()
    .update(User)
    .set({ password: hashedPassword  })
    .where("email = :email", { email })
    .execute();
  } catch(e) {
    throw new HTTP400Error(e);
  }
};