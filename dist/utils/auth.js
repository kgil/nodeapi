"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const token_1 = require("./token");
const authValidate = (accessToken) => __awaiter(this, void 0, void 0, function* () {
    if (accessToken && accessToken.startsWith('Bearer ')) {
        accessToken = accessToken.slice(7, accessToken.length);
        return yield token_1.validateToken(accessToken);
    }
    else {
        return {
            flag: false,
            role: '',
            id: 0
        };
    }
});
exports.default = authValidate;
