"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const commonSettings_1 = require("@config/commonSettings");
const CHECKS_USER = __importStar(require("./checks"));
const TOKEN_ACTIONS = __importStar(require("@utils/token"));
const auth_1 = __importDefault(require("@middleware/auth"));
exports.default = [
    {
        path: `${commonSettings_1.API_URL}session`,
        method: 'post',
        handler: [
            CHECKS_USER.checkRequiredFieldsStartSession,
            CHECKS_USER.checkDataFromReq,
            CHECKS_USER.checkExistingUser,
            CHECKS_USER.checkPasswordUser,
            (req, res) => {
                const tokens = TOKEN_ACTIONS.createTokens(req.user.id, req.user.role);
                res.status(200).send(res.json({ tokens }));
            }
        ]
    },
    {
        path: `${commonSettings_1.API_URL}session`,
        method: 'put',
        handler: [
            CHECKS_USER.checkRequiredFieldsRefreshSession,
            CHECKS_USER.checkTokens,
            (req, res) => __awaiter(this, void 0, void 0, function* () {
                const accessToken = req.body.accessToken;
                const refreshToken = req.body.refreshToken;
                const tokens = yield TOKEN_ACTIONS.refreshTokens(accessToken, refreshToken);
                res.status(200).send(res.json({ tokens }));
            })
        ]
    },
    {
        path: `${commonSettings_1.API_URL}session`,
        method: 'delete',
        handler: [
            auth_1.default,
            CHECKS_USER.checkRequiredFieldsDeleteSession,
            (req, res) => {
                const refreshToken = req.body.refreshToken;
                TOKEN_ACTIONS.deleteToken(refreshToken);
                res.status(200).send();
            }
        ]
    }
];
