"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const User_1 = __importDefault(require("@entity/User"));
const Profile_1 = __importDefault(require("@entity/Profile"));
const findProfileMethods_1 = require("./findProfileMethods");
const createProfile = (body, id) => __awaiter(this, void 0, void 0, function* () {
    const profile = yield typeorm_1.getRepository(Profile_1.default).create({
        firstName: body.firstName,
        lastName: body.lastName,
        phone: body.phone,
        middleName: body.middleName,
        gender: body.gender,
        bornCountry: body.bornCountry,
        birthDate: body.birthDate,
        currentCountry: body.currentCountry,
        maritalStatus: body.maritalStatus,
        countChildren: body.countChildren,
        occupation: body.occupation,
        levelEducation: body.levelEducation,
        photo: body.photo
    }).save();
    yield typeorm_1.getConnection()
        .createQueryBuilder()
        .relation(User_1.default, 'profile')
        .of({ id })
        .set(profile);
    return yield findProfileMethods_1.getProfile(id);
});
exports.default = createProfile;
