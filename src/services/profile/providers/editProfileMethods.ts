import { BodyProfile } from '../types';
import { getConnection, getRepository } from 'typeorm';
import Profile from '@entity/Profile';
import User from '@entity/User';

const getProfileId = async (id: number) => {
  const profile = await getRepository(User)
  .find({ select: ['id', 'profile'], relations: ['profile'], where: { id }, take: 1 });
  return profile[0].profile.id;
}

export const editFullProfile = async (body: BodyProfile, id: number) => {
  const idProfile = await getProfileId(id);

  await getConnection()
    .createQueryBuilder()
    .update(Profile)
    .set({
      firstName: body.firstName,
      lastName: body.lastName,
      phone: body.phone,  
      middleName: body.middleName,
      gender: body.gender,
      bornCountry: body.bornCountry,
      birthDate: body.birthDate,
      currentCountry: body.currentCountry,
      maritalStatus: body.maritalStatus,
      countChildren: body.countChildren,
      occupation: body.occupation,
      levelEducation: body.levelEducation,
      photo: body.photo
    })
    .where("id = :id", { id: idProfile })
    .execute();
};

export const editPatchProfile = async (params: any, id: number) => {
  const idProfile = await getProfileId(id);

  for (const key in params) {
    await getConnection()
    .createQueryBuilder()
    .update(Profile)
    .set({  
      [key]: params[key]
    })
    .where("id = :id", { id: idProfile })
    .execute();
  }
};