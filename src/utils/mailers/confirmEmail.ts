import mail from '@utils/mail';
import createTemplateEmail from '@utils/createTemplateEmail';
import { FULL_URL } from '@config/commonSettings';
import dotenv from 'dotenv';
import * as JWT from 'jsonwebtoken';

dotenv.config();

declare var process : {
  env: {
    SECRET_KEY: string,  
  }
}

interface User {
  email: string;
  password: string;
}

const SECRET_KEY = process.env.SECRET_KEY;

export const sendEncodingUser = async (user: User) => {
  const emailToken = JWT.sign(
    { 
      email: user.email,
      password: user.password
    },
    SECRET_KEY,
    { expiresIn: '1h' }
  );

  const confirmEmailLink = `${FULL_URL}confirmEmail/${emailToken}`;

  const options = await createTemplateEmail('confirmEmail.hbs', 
    {
      email: user.email,
      subjectEmail: 'Confirm Email',
    },
    { 
      confirmEmailLink 
    }
  );
  
  mail(options);
}

export const decodingUser = (token: string) => {
  let data = {
    email: '',
    password: ''
  };

  JWT.verify(token, SECRET_KEY, (err: JWT.VerifyErrors, decoded: any) => {
    if (err) {
      return false;
    } else {
      data = {
        email: decoded.email,
        password: decoded.password
      }
    }
  });

  return data;
}