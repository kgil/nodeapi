import dotenv from 'dotenv';
import * as JWT from 'jsonwebtoken';
import { HTTP440Error, HTTP400Error, HTTP401Error } from './errors/httpErrors';
import { redisClient } from '../server';
import { UserRole } from '@entity/User';

dotenv.config();

declare var process : {
  env: {
    SECRET_KEY: string,
    SECRET_REFRESH_KEY: string
  }
}

const SECRET_KEY = process.env.SECRET_KEY;
const SECRET_REFRESH_KEY = process.env.SECRET_REFRESH_KEY;
const EXPIRED_ACCES_TOKEN_TIME = 700;
const EXPIRED_REFRESH_TOKEN_TIME = 7000;

export const createTokens = (id: number, role: string = UserRole.GHOST) => {
  const refreshToken = generateRefreshToken(128);
  const accessToken = JWT.sign({ id, role },
    SECRET_KEY,
    { expiresIn: EXPIRED_ACCES_TOKEN_TIME }
  );
  redisClient.setex(refreshToken, EXPIRED_REFRESH_TOKEN_TIME, `${String(id)},${String(role)}`);
  return {
    accessToken,
    refreshToken
  };
}

interface UserData {
  flag: boolean;
  role: string;
  id: number;
}

export const  validateToken = (accessToken: string): Promise<UserData> => {
  return new Promise((resolve) => {
    JWT.verify(accessToken, SECRET_KEY, (err: JWT.VerifyErrors, decoded: any) => {
        if (err) {
          if (err.name === 'TokenExpiredError') {
            throw new HTTP440Error();
          } else {
            throw new HTTP401Error();
          } 
        } else {
          const data: UserData = {
            flag: true,
            role: decoded.role,
            id: decoded.id
          }
          resolve(data);
        }
      });
    });
};

interface Tokens {
  accessToken: string;
  refreshToken: string;
}

export const  refreshTokens = (accessToken: string, refreshToken: string): Promise<Tokens | null> => {
  return new Promise((resolve) => {
    JWT.verify(accessToken, SECRET_KEY, async (err: JWT.VerifyErrors) => {
      if (err) {
        if (err.name === 'TokenExpiredError') {
          const data = await helpRefreshToken(refreshToken);
          resolve(data);
        } else {
          throw new HTTP401Error();
        } 
      } else {
        const data = {
          accessToken,
          refreshToken
        }
        resolve(data);
      }
    });
  });
}

export const deleteToken = (refreshToken: string) => {
  redisClient.del(refreshToken);
}

const helpRefreshToken = async (refreshToken: string): Promise<Tokens | null> => {
  try {
    const response = await redisClient.getAsync(refreshToken);
    const arrResponse = response.split(',');
    const id: number = Number(arrResponse[0]);
    const role: string = arrResponse[1];
    redisClient.setex(refreshToken, 20, response);
    return createTokens(id, role);
  } catch(e) {
    return null;
  }
}

const generateRefreshToken = (length: number) => {
  let text = '';
  const charset = SECRET_REFRESH_KEY;
  for (var i = 0; i < length; i++)
    text += charset.charAt(Math.floor(Math.random() * charset.length));
  return text;
}