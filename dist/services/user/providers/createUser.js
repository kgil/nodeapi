"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const User_1 = __importStar(require("@entity/User"));
const METHODS = __importStar(require("password-hash"));
const createUser = (body, admin = false) => __awaiter(this, void 0, void 0, function* () {
    let { email, password } = body;
    password = password.trim();
    email = email.toLowerCase().trim();
    const hashedPassword = METHODS.generate(password);
    const user = yield typeorm_1.getRepository(User_1.default).create({
        email,
        password: hashedPassword,
        role: admin ? User_1.UserRole.ADMIN : User_1.UserRole.GHOST
    }).save();
    return user;
});
exports.default = createUser;
