import { Request } from 'express';

interface BodyUser {
  email: string;
  password: string;
}

interface ParamsUser {
  id: number;
  password: string;
  role: string;
}

export const createTokenKeys = [
  'email',
  'password'
]

export const refreshTokenKeys = [
  'accessToken',
  'refreshToken'
]

export const deleteTokenKeys = [
  'refreshToken'
]

export interface AuthRequest extends Request {
  body: BodyUser;
  user: ParamsUser;
}

interface Tokens {
  accessToken: string;
  refreshToken: string;
}

interface refreshToken {
  refreshToken: string;
}

export interface refreshRequest extends  Request {
  body: Tokens;
}

export interface deleteRequest extends  Request {
  body: refreshToken;
}