"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const commonSettings_1 = require("@config/commonSettings");
const CHECKS_USER = __importStar(require("./checks"));
const CONFIRM_EMAIL = __importStar(require("./providers/createUserWithConfirm"));
const RESET_PASSWORD = __importStar(require("./providers/resetPass"));
const createUser_1 = __importDefault(require("./providers/createUser"));
const authAdmin_1 = __importDefault(require("@middleware/authAdmin"));
const USER_FIND_METHODS = __importStar(require("./providers/findUserMethods"));
exports.default = [
    {
        path: `${commonSettings_1.API_URL}user/create/admin`,
        method: 'post',
        handler: [
            CHECKS_USER.checkRequiredFieldsCreateUser,
            CHECKS_USER.checkDataFromReq,
            CHECKS_USER.checkUserForUnique,
            ({ body }, res) => __awaiter(this, void 0, void 0, function* () {
                const user = yield createUser_1.default(body, true);
                res.status(200).send(res.json({ id: user.id }));
            })
        ]
    },
    {
        path: `${commonSettings_1.API_URL}user/create`,
        method: 'post',
        handler: [
            CHECKS_USER.checkRequiredFieldsCreateUser,
            CHECKS_USER.checkDataFromReq,
            CHECKS_USER.checkUserForUnique,
            ({ body }, res) => {
                CONFIRM_EMAIL.sendUser(body);
                res.status(200).send();
            }
        ]
    },
    {
        path: `${commonSettings_1.API_URL}confirmEmail/:token`,
        method: 'get',
        handler: [
            ({ params }, res) => __awaiter(this, void 0, void 0, function* () {
                const user = CONFIRM_EMAIL.confirmEmail(params.token);
                yield CONFIRM_EMAIL.createUserAfterConfirm(user);
                res.status(200).send();
            })
        ]
    },
    {
        path: `${commonSettings_1.API_URL}user/forgot`,
        method: 'post',
        handler: [
            CHECKS_USER.checkRequiredFieldsForSendReset,
            CHECKS_USER.checkEmail,
            CHECKS_USER.checkExistingUser,
            (req, res) => {
                RESET_PASSWORD.sendEmail(req.body.email);
                res.status(200).send();
            }
        ]
    },
    {
        path: `${commonSettings_1.API_URL}passwordReset/:token`,
        method: 'post',
        handler: [
            CHECKS_USER.checkRequiredFieldsForResetPassword,
            CHECKS_USER.checkEqualPassword,
            ({ body, params }, res) => __awaiter(this, void 0, void 0, function* () {
                const email = RESET_PASSWORD.confirmEmail(params.token);
                yield RESET_PASSWORD.resetPasswordAfterConfirm(email, body.password);
                res.status(200).send();
            })
        ]
    },
    {
        path: `${commonSettings_1.API_URL}user/get`,
        method: 'get',
        handler: [
            CHECKS_USER.checkSearchParams,
            authAdmin_1.default,
            (req, res) => __awaiter(this, void 0, void 0, function* () {
                const user = yield USER_FIND_METHODS.findUserById(req.query.id);
                res.status(200).send(res.json({ user }));
            })
        ]
    },
    {
        path: `${commonSettings_1.API_URL}user/get/all`,
        method: 'get',
        handler: [
            authAdmin_1.default,
            (req, res) => __awaiter(this, void 0, void 0, function* () {
                const users = yield USER_FIND_METHODS.findAllUsers();
                res.status(200).send(res.json({ users }));
            })
        ]
    }
];
