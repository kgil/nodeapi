import { getRepository } from 'typeorm';
import User from '@entity/User';

export const findUserById = async (id: number) => {
  return await getRepository(User)
    .find({ select: ['id', 'email', 'profile'], where: { id } });
};

export const findAllUsers = async() => {
  return await getRepository(User)
  .find({ select: ['id', 'email', 'profile'], relations: ['profile']});
};