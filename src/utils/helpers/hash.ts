import * as METHODS from 'password-hash';

const hash = (password: string) => {
  return METHODS.generate(password);
};

console.log(hash('hwz4im'));