"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const mail_1 = __importDefault(require("@utils/mail"));
const createTemplateEmail_1 = __importDefault(require("@utils/createTemplateEmail"));
const commonSettings_1 = require("@config/commonSettings");
const dotenv_1 = __importDefault(require("dotenv"));
const JWT = __importStar(require("jsonwebtoken"));
dotenv_1.default.config();
const SECRET_KEY = process.env.SECRET_KEY;
exports.sendEncodingEmail = (email) => __awaiter(this, void 0, void 0, function* () {
    const emailToken = JWT.sign({
        email
    }, SECRET_KEY, { expiresIn: '1h' });
    const confirmEmailLink = `${commonSettings_1.FULL_URL}passwordReset/${emailToken}`;
    const options = yield createTemplateEmail_1.default('resetPass.hbs', {
        email,
        subjectEmail: 'Reset password',
    }, {
        confirmEmailLink
    });
    mail_1.default(options);
});
exports.decodingEmail = (token) => {
    let email = '';
    JWT.verify(token, SECRET_KEY, (err, decoded) => {
        if (err) {
            return false;
        }
        email = decoded.email;
    });
    return email;
};
