"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const httpErrors_1 = require("@utils/errors/httpErrors");
const User_1 = __importDefault(require("@entity/User"));
const typeorm_1 = require("typeorm");
const types_1 = require("./types");
const checkRequiredFields_1 = __importDefault(require("@utils/checkRequiredFields"));
exports.checkDataFromReq = ({ body }, res, next) => {
    Object.values(body).map(item => {
        if (!item) {
            throw new httpErrors_1.HTTP400Error('You must provide all necessery information');
        }
        return;
    });
    next();
};
exports.checkRequiredFields = ({ body }, res, next) => {
    checkRequiredFields_1.default(types_1.bodyProfileKeys, body);
    next();
};
exports.checkRequiredFields;
exports.checkRequiredFields;
exports.checkProfileForUnique = (req, res, next) => __awaiter(this, void 0, void 0, function* () {
    const users = yield typeorm_1.getRepository(User_1.default)
        .find({ select: ['id', 'profile'], relations: ['profile'], where: { id: req.id }, take: 1 });
    if (users[0].profile) {
        throw new httpErrors_1.HTTP400Error('Profile already exist. You can only updating that');
    }
    else {
        next();
    }
});
