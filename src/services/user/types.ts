import { Request } from 'express';

export interface BodyUser {
  email: string;
  password: string;
}

export interface BodyEmail {
  email: string;
}

export interface BodyPasswords {
  email?: string;
  password: string;
  confirmPassword: string;
}

export const bodyUserKeys = [
  'email',
  'password'
]

export const sendForResetKeys = [
  'email'
]

export const checkPasswordKeys = [
  'password',
]

export const resetPasswordKeys = [
  'password',
  'confirmPassword'
]

export interface UserRequest extends Request {
  body: BodyUser;
}

export interface SendForResetRequest extends Request {
  body: BodyEmail;
}

export interface ResetPasswordRequest extends Request {
  body: BodyPasswords;
}

