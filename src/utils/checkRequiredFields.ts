import { HTTP400Error } from '@utils/errors/httpErrors';

function checkBodyForm<T> (form: string[], body: T) {
  form.map(item => {
    if (!(item in body)) {
      throw new HTTP400Error(`Missing ${item} key`);
    }
    return;
  });
}

export default checkBodyForm;