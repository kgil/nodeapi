import { Response, NextFunction } from 'express';
import { HTTP400Error } from '@utils/errors/httpErrors';
import User from '@entity/User';
import { getRepository } from 'typeorm';
import { ProfileRequest, bodyProfileKeys } from './types';
import checkBodyForm from '@utils/checkRequiredFields';

export const checkDataFromReq = (
  { body }: ProfileRequest,
  res: Response,
  next: NextFunction
) => {
  Object.values(body).map(item => {
    if (!item) {
      throw new HTTP400Error('You must provide all necessery information');
    }
    return;
  });
  next();
};

export const checkRequiredFields = (
  { body }: ProfileRequest,
  res: Response,
  next: NextFunction
) => {
  checkBodyForm(bodyProfileKeys, body)
  next();
};


checkRequiredFields
checkRequiredFields

export const checkProfileForUnique = async (
  req: ProfileRequest,
  res: Response,
  next: NextFunction
) => {
  const users = await getRepository(User)
  .find({ select: ['id', 'profile'], relations: ['profile'], where: { id: req.id }, take: 1 });

  if (users[0].profile) {
    throw new HTTP400Error('Profile already exist. You can only updating that');
  } else {
    next();
  }
};