import { BodyProfile } from '../types';
import { getRepository, getConnection } from 'typeorm';
import User from '@entity/User';
import Profile from '@entity/Profile';
import { getProfile } from './findProfileMethods';

const createProfile = async (body: BodyProfile, id: number) => {
    const profile = await getRepository(Profile).create({
      firstName: body.firstName,
      lastName: body.lastName,
      phone: body.phone,
      middleName: body.middleName,
      gender: body.gender,
      bornCountry: body.bornCountry,
      birthDate: body.birthDate,
      currentCountry: body.currentCountry,
      maritalStatus: body.maritalStatus,
      countChildren: body.countChildren,
      occupation: body.occupation,
      levelEducation: body.levelEducation,
      photo: body.photo
    }).save();

    await getConnection()
      .createQueryBuilder()
      .relation(User, 'profile')
      .of({ id })
      .set(profile);

    return await getProfile(id);
};

export default createProfile;