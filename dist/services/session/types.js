"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createTokenKeys = [
    'email',
    'password'
];
exports.refreshTokenKeys = [
    'accessToken',
    'refreshToken'
];
exports.deleteTokenKeys = [
    'refreshToken'
];
