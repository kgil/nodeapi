"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const redis = require("redis");
const bluebird_1 = __importDefault(require("bluebird"));
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config();
const DBR_PORT = process.env.DBR_PORT;
const DB_HOST = process.env.DB_HOST;
bluebird_1.default.promisifyAll(redis.RedisClient.prototype);
bluebird_1.default.promisifyAll(redis.Multi.prototype);
function startRedis() {
    const redisClient = redis.createClient(DBR_PORT, DB_HOST);
    redisClient.on('connect', () => {
        console.log('Redis plugged in.');
    });
    redisClient.on('error', function (err) {
        console.error('Error connecting to redis', err);
    });
    return redisClient;
}
exports.default = startRedis;
