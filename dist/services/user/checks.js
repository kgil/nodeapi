"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const httpErrors_1 = require("@utils/errors/httpErrors");
const User_1 = __importDefault(require("@entity/User"));
const types_1 = require("./types");
const regexps_1 = __importDefault(require("@config/regexps"));
const checkRequiredFields_1 = __importDefault(require("@utils/checkRequiredFields"));
exports.checkUserForUnique = (req, res, next) => __awaiter(this, void 0, void 0, function* () {
    const email = req.body.email.toLowerCase();
    const existingUser = yield typeorm_1.getRepository(User_1.default)
        .createQueryBuilder('user')
        .where('user.email = :email', { email })
        .getOne();
    if (existingUser) {
        throw new httpErrors_1.HTTP400Error('This email already in used');
    }
    else {
        next();
    }
});
exports.checkDataFromReq = ({ body }, res, next) => {
    const { email, password } = body;
    Object.values(body).map(item => {
        if (!item) {
            throw new httpErrors_1.HTTP400Error('You must provide all necessery information');
        }
    });
    if (!email.match(regexps_1.default.email)) {
        throw new httpErrors_1.HTTP400Error('Email is not valid');
    }
    if (password.length < 6 || password.length > 128) {
        throw new httpErrors_1.HTTP400Error('Password should be between 6 and 128 characters');
    }
    next();
};
exports.checkSearchParams = (req, res, next) => {
    if (!req.query.id) {
        throw new httpErrors_1.HTTP400Error('Missing id param');
    }
    else {
        next();
    }
};
exports.checkEmail = (req, res, next) => {
    const email = req.body.email.toLowerCase().trim();
    if (!email) {
        throw new httpErrors_1.HTTP400Error('You must provide all necessery information');
    }
    else if (!email.match(regexps_1.default.email)) {
        throw new httpErrors_1.HTTP400Error('Email is not valid');
    }
    else {
        next();
    }
};
exports.checkExistingUser = (req, res, next) => __awaiter(this, void 0, void 0, function* () {
    const email = req.body.email.toLowerCase().trim();
    const existingUser = yield typeorm_1.getRepository(User_1.default)
        .createQueryBuilder('user')
        .where('user.email = :email', { email })
        .getOne();
    if (!existingUser) {
        throw new httpErrors_1.HTTP400Error('There is not such user');
    }
    else {
        next();
    }
});
exports.checkEqualPassword = (req, res, next) => {
    const password = req.body.password.trim();
    const confirmPassword = req.body.confirmPassword.trim();
    if (password !== confirmPassword) {
        throw new httpErrors_1.HTTP400Error('Password isnt equal');
    }
    else {
        next();
    }
};
exports.checkRequiredFieldsCreateUser = ({ body }, res, next) => {
    checkRequiredFields_1.default(types_1.bodyUserKeys, body);
    next();
};
exports.checkRequiredFieldsForSendReset = ({ body }, res, next) => {
    checkRequiredFields_1.default(types_1.sendForResetKeys, body);
    next();
};
exports.checkRequiredFieldsForResetPassword = ({ body }, res, next) => {
    checkRequiredFields_1.default(types_1.resetPasswordKeys, body);
    next();
};
