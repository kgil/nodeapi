import mail from '@utils/mail';
import createTemplateEmail from '@utils/createTemplateEmail';
import { FULL_URL } from '@config/commonSettings';
import { HTTP400Error } from '@utils/errors/httpErrors';
import dotenv from 'dotenv';
import * as JWT from 'jsonwebtoken';

dotenv.config();

declare var process : {
  env: {
    SECRET_KEY: string,  
  }
}

const SECRET_KEY = process.env.SECRET_KEY;

export const sendEncodingEmail = async (email: string) => {
  const emailToken = JWT.sign(
    { 
      email
    },
    SECRET_KEY,
    { expiresIn: '1h' }
  );

  const confirmEmailLink = `${FULL_URL}passwordReset/${emailToken}`;

  const options = await createTemplateEmail('resetPass.hbs', 
    {
      email,
      subjectEmail: 'Reset password',
    },
    { 
      confirmEmailLink 
    }
  );
  
  mail(options);
}

export const decodingEmail = (token: string) => {
  let email: string = '';
  JWT.verify(token, SECRET_KEY, (err: JWT.VerifyErrors, decoded: any) => {
    if (err) {
      return false;
    }
    email = decoded.email;
  });
  return email;
}