import { Request, Response, NextFunction } from 'express';
import { getRepository } from 'typeorm';
import { HTTP400Error } from '@utils/errors/httpErrors';
import * as METHODS from 'password-hash';
import User from '@entity/User';
import { bodyUserKeys, 
  sendForResetKeys, 
  resetPasswordKeys, 
  ResetPasswordRequest, 
  UserRequest, 
  SendForResetRequest, 
  checkPasswordKeys } from './types';
import REGEXPS from '@config/regexps';
import checkBodyForm from '@utils/checkRequiredFields';

export const checkUserForUnique = async (
  req: UserRequest,
  res: Response,
  next: NextFunction
) => {
  const email: string = req.body.email.toLowerCase();

  const existingUser = await getRepository(User)
    .createQueryBuilder('user')
    .where('user.email = :email', { email })
    .getOne();

  if (existingUser) {
    throw new HTTP400Error('This email already in used');
  } else {
    next();
  }
};

export const checkDataFromReq = (
  { body }: UserRequest,
  res: Response,
  next: NextFunction
) => {
  const { email, password } = body;

  Object.values(body).map(item => {
    if (!item) {
      throw new HTTP400Error('You must provide all necessery information');
    }
  });

  if (!email.match(REGEXPS.email)) {
    throw new HTTP400Error('Email is not valid');
  }

  if (password.length < 6 || password.length > 128) {
    throw new HTTP400Error('Password should be between 6 and 128 characters');
  }
  next();
};

export const checkSearchParams = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  if (!req.query.id) {
    throw new HTTP400Error('Missing id param');
  } else {
    next();
  }
};

export const checkEmail = (
  req: SendForResetRequest,
  res: Response,
  next: NextFunction
) => {
  const email: string = req.body.email.toLowerCase().trim();
  if (!email) {
    throw new HTTP400Error('You must provide all necessery information');
  } else if (!email.match(REGEXPS.email)) {
    throw new HTTP400Error('Email is not valid');
  } else {
    next();
  }
};

export const checkExistingUser = async (
  req: SendForResetRequest,
  res: Response,
  next: NextFunction
) => {
  const email: string = req.body.email.toLowerCase().trim();

  const existingUser = await getRepository(User)
    .createQueryBuilder('user')
    .where('user.email = :email', { email })
    .getOne();

  if (!existingUser) {
    throw new HTTP400Error('There is not such user');
  } else {
    next();
  }
};

export const checkEqualPasswords = (
  req: ResetPasswordRequest,
  res: Response,
  next: NextFunction
) => {
  const password: string = req.body.password.trim();
  const confirmPassword: string = req.body.confirmPassword.trim();
  if (password !== confirmPassword) {
    throw new HTTP400Error('Password isnt equal');
  } else {
    next();
  }
};

export const checkPassword = async (
  req: ResetPasswordRequest,
  res: Response,
  next: NextFunction
) => {
  const email = req.body.email;

  const user = await getRepository(User)
    .createQueryBuilder('user')
    .where('user.email = :email', { email })
    .getOne();

  if (user) {
    const password: string = req.body.password.trim();
    const passwordFromDB: string = user.password;

    if (!METHODS.verify(password, passwordFromDB)) {
      throw new HTTP400Error('Password is not valid');
    } else {
      next();
    }
  }
};

export const checkRequiredFieldsCreateUser = (
  { body }: Request,
  res: Response,
  next: NextFunction
) => {
  checkBodyForm(bodyUserKeys, body);
  next();
};

export const checkRequiredFieldsForSendReset = (
  { body }: Request,
  res: Response,
  next: NextFunction
) => {
  checkBodyForm(sendForResetKeys, body);
  next();
};

export const checkRequiredFieldsForResetPasswordWithoutProfile = (
  { body }: Request,
  res: Response,
  next: NextFunction
) => {
  checkBodyForm(resetPasswordKeys, body);
  next();
};

export const checkRequiredFieldsForResetPasswordWithProfile = (
  { body }: Request,
  res: Response,
  next: NextFunction
) => {
  checkBodyForm(checkPasswordKeys, body);
  next();
};