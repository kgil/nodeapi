import redis = require('redis');
import bluebird from 'bluebird';
import dotenv from 'dotenv';

dotenv.config();

declare var process : {
  env: {
    DBR_PORT: number,
    DB_HOST: string
  }
}

const DBR_PORT = process.env.DBR_PORT;
const DB_HOST = process.env.DB_HOST;

bluebird.promisifyAll((<any>redis).RedisClient.prototype);
bluebird.promisifyAll((<any>redis).Multi.prototype);

declare module "redis" {
  export interface RedisClient extends NodeJS.EventEmitter {
      setAsync(key:string, value:string): Promise<void>;
      getAsync(key:string): Promise<string>;
  }
}

function startRedis() {
  const redisClient = redis.createClient(DBR_PORT, DB_HOST);

  redisClient.on('connect', () => {
    console.log('Redis plugged in.');
  });
  redisClient.on('error', function(err) {
    console.error('Error connecting to redis', err);
  });
  return redisClient;
}

export default startRedis;