import handlebars from 'handlebars';
import { SUPPORT_EMAIL } from '@config/commonSettings';
import readFile from './helpers/readFile';

interface Email {
  email: string;
  subjectEmail: string;
}

const createTemplateEmail = async (nameFile: string, options: Email, localVariablesTemplate: any) => {
  const source = await readFile('dist/views/emailTemplates', nameFile);
  const template = handlebars.compile(source);

  return {
    from: `"Green Card USA" <${SUPPORT_EMAIL}>`,
    to: options.email,
    subject: `Green Card USA | ${options.subjectEmail}`,
    html: template(localVariablesTemplate)
  };
};

export default createTemplateEmail;