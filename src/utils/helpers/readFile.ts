import fs from 'fs';
import path from 'path';

const readFile = (filePath: string, nameFile: string) => {
  return new Promise((resolve, reject) => {
    fs.readFile(path.join(`${process.cwd()}/${filePath}`, nameFile), 'utf8', function(err, source) {
      if (err) {
        reject(err);
      }
      resolve(source);
    });
  });
}

export default readFile;