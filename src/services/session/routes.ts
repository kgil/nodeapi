import { API_URL } from '@config/commonSettings';
import { Response } from 'express';
import { AuthRequest, refreshRequest, deleteRequest } from './types';
import * as CHECKS_USER from './checks';
import * as TOKEN_ACTIONS from '@utils/token';
import auth from '@middleware/auth';

export default [
  {
    path: `${API_URL}session`,
    method: 'post',
    handler: [
      CHECKS_USER.checkRequiredFieldsStartSession,
      CHECKS_USER.checkDataFromReq,
      CHECKS_USER.checkExistingUser,
      CHECKS_USER.checkPasswordUser,
      (req: AuthRequest, res: Response) => {
        const tokens = TOKEN_ACTIONS.createTokens(req.user.id, req.user.role);
        res.status(200).send(res.json({ tokens }));
      }
    ]
  },
  {
    path: `${API_URL}session`,
    method: 'put',
    handler: [
      CHECKS_USER.checkRequiredFieldsRefreshSession,
      CHECKS_USER.checkTokens,
      async (req: refreshRequest, res: Response) => {
        const accessToken = req.body.accessToken;
        const refreshToken = req.body.refreshToken;
        const tokens = await TOKEN_ACTIONS.refreshTokens(accessToken, refreshToken);
        res.status(200).send(res.json({ tokens }));
      }
    ]
  },
  {
    path: `${API_URL}session`,
    method: 'delete',
    handler: [
      auth,
      CHECKS_USER.checkRequiredFieldsDeleteSession,
      (req: deleteRequest, res: Response) => {
        const refreshToken = req.body.refreshToken;
        TOKEN_ACTIONS.deleteToken(refreshToken);
        res.status(200).send();
      }
    ]
  }
];

