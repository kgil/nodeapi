"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const httpErrors_1 = require("@utils/errors/httpErrors");
const auth_1 = __importDefault(require("@utils/auth"));
const User_1 = require("@entity/User");
const auth = (req, res, next) => __awaiter(this, void 0, void 0, function* () {
    if (req.headers.authorization) {
        const obj = yield auth_1.default(req.headers.authorization);
        if (obj.id && obj.flag && obj.role === User_1.UserRole.GHOST) {
            req.id = obj.id;
            next();
        }
        else {
            throw new httpErrors_1.HTTP401Error();
        }
    }
    else {
        throw new httpErrors_1.HTTP401Error();
    }
});
exports.default = auth;
