import { API_URL } from '@config/commonSettings';
import { Response } from 'express';
import { ProfileRequest } from './types';
import auth, { AuthRequestWithId } from '@middleware/auth';
import * as CHECKS_USER from './checks';
import createProfile from './providers/createProfile';
import { editFullProfile, editPatchProfile } from './providers/editProfileMethods';
import { getProfile } from './providers/findProfileMethods';

export default [
  {
    path: `${API_URL}profile/create`,
    method: 'post',
    handler: [
      auth,
      CHECKS_USER.checkProfileForUnique,
      CHECKS_USER.checkRequiredFields,
      CHECKS_USER.checkDataFromReq,
      async (req: ProfileRequest, res: Response) => {
        const profile = await createProfile(req.body, req.id);
        res.status(200).send(res.json({ profile }));
      } 
    ]
  },
  {
    path: `${API_URL}profile/get`,
    method: 'get',
    handler: [
      auth,
      async (req: AuthRequestWithId, res: Response) => {
        const profile = await getProfile(req.id);
        res.status(200).send(res.json({ profile }));
      }
    ]
  },
  {
    path: `${API_URL}profile/edit`,
    method: 'put',
    handler: [
      auth,
      CHECKS_USER.checkDataFromReq,
      async (req: ProfileRequest, res: Response) => {
        await editFullProfile(req.body, req.id);
        res.status(200).send();
      }
    ]
  },
  {
    path: `${API_URL}profile/edit`,
    method: 'patch',
    handler: [
      auth,
      CHECKS_USER.checkRequiredFields,
      CHECKS_USER.checkDataFromReq,
      async (req: ProfileRequest, res: Response) => {
        await editPatchProfile(req.body, req.id);
        res.status(200).send();
      }
    ]
  }
];